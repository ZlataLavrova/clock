onload = () => {
    const canvas = document.createElement('canvas');
    let ctx = canvas.getContext('2d');

    canvas.width = 600;
    canvas.height = 600;
    ctx.fillStyle = 'black';
    ctx.fillRect(0, 0, canvas.width, canvas.height);


    function drawFrame(t) {
        //переменные для часов/минут/секунд
        let now = new Date()
        let sec = now.getSeconds();
        let min = now.getMinutes();
        let hr = now.getHours();

        //дизайн
        ctx.setTransform();
        ctx.fillStyle = 'black';
        ctx.fillRect(0, 0, 600, 600);
        ctx.lineWidth = 10;
        ctx.fillStyle = 'black';


        // рисовка меток на круге
        for (let i = 0; i < 60; i += 5){
            let cos = Math.cos(6* i * (Math.PI/180));
            let sin = Math.sin(6* i * (Math.PI/180));
            ctx.setTransform(cos, sin, -sin, cos, canvas.width/2, canvas.height/2);
            ctx.fillStyle = 'white';
            ctx.fillRect(254, -10, 30, 20);
            ctx.transform(1, 0, 0, 1, 30, 1);
        }

        //секундная стрелка
        let cos = Math.cos(6* sec * (Math.PI/180));
        let sin = Math.sin(6* sec * (Math.PI/180));
        ctx.setTransform(cos, sin, -sin, cos, canvas.width/2, canvas.height/2);
        ctx.fillStyle = 'red';
        for (let i = 0; i < 8; i += 1){
            ctx.fillRect(0, -10, 30, 18 - 2*i);
            ctx.transform(1, 0, 0, 1, 30, 1);
        }

        //минутная стрелка
        cos = Math.cos(6* (min + sec/60) * (Math.PI/180));
        sin = Math.sin(6* (min + sec/60) * (Math.PI/180));
        ctx.setTransform(cos, sin, -sin, cos, canvas.width/2, canvas.height/2);
        ctx.fillStyle = 'white';
        for (let i = 0; i < 7; i += 1){
            ctx.fillRect(0, -10, 50, 20 - 2.5*i);
            ctx.transform(1, 0, 0, 1, 25, 1);
        }

        //часовая стрелка
        cos = Math.cos(30* (hr + min/60) * (Math.PI/180));
        sin = Math.sin(30* (hr + min/60) * (Math.PI/180));
        ctx.setTransform(cos, sin, -sin, cos, canvas.width/2, canvas.height/2);
        ctx.fillStyle = 'white';
        for (let i = 0; i < 6; i += 1){
            ctx.fillRect(0, -10, 30, 20 - 3*i);
            ctx.transform(1, 0, 0, 1, 15, 1);
        }

        requestAnimationFrame(drawFrame);
    }
    requestAnimationFrame(drawFrame);
    canvas.style.transform = "rotate(270deg)";
    document.body.appendChild(canvas);
}